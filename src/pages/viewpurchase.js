import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import styled from "styled-components";

// basic card component:
const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const RestaurantCard = styled(Card)`
  padding: 11px 18px;
  padding-top: 20px;
  display: flex;
  justify-content: space-between;

  .restaurant-profile-wrapper {
    display: flex;
    align-items: center;

    .restaurant-profile {
      margin-left: 14px;

      h2 {
        font-size: 18px;
        font-weight: 700;
      }

      .button {
        width: 77px;
        background-color: #4a494a;
        border-radius: 50px;
        color: white;
        font-size: 13px;
        text-align: center;
        padding: 3px 0px;
      }

      p {
        font-size: 13px;
        font-weight: 700;
        margin: 5px 0;
      }
    }
  }

  .restaurant-location {
    margin-top: 9px;
    font-size: 11px;
    font-weight: 500;

    p {
      line-height: 22px;
      margin-top: 5px;
      font-weight: 400;
    }
  }

  .restaurant-price {
    text-align: right;

    .price {
      color: #a2a2a2;
      font-size: 12px;
      font-weight: 600;
      line-height: 17px;

      b {
        color: #000000;
        font-size: 20px;
        font-weight: 700;
      }

      p {
        margin-top: 5px;
      }
    }

    .redeem-button-wrapper {
      width: 98px;
      border-radius: 12px;
      text-align: center;

      p {
        background-color: #9f9c9c;
        font-size: 8px;
        font-weight: 500;
        text-transform: uppercase;
        padding: 3px;
        color: #ffffff;
        border-radius: 8px;
      }

      .redeem-button {
        border-radius: 14px;
        background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
        padding: 9px 0;
        font-size: 18px;
        font-weight: 700;
        color: white;
        margin-top: 20px;
      }
    }
  }
`;

function Restaurant(props) {
  return (
    <RestaurantCard>
      <div className="restaurant-intro">
        <div className="restaurant-profile-wrapper">
          <img src={props.img} alt={props.title} />
          <div className="restaurant-profile">
            <h2>{props.title}</h2>
            <p>{props.twitter}</p>
            <div className="button">profile</div>
          </div>
        </div>

        <div className="restaurant-location">
          <b>{props.location}</b>
          <p>more details…</p>
        </div>
      </div>

      <div className="restaurant-price">
        <div className="price">
          <b>{props.price}</b>
          <p>for $100</p>
        </div>
        <div className="redeem-button-wrapper">
          <p>{props.open}</p>
          <div className="redeem-button">Redeem</div>
        </div>
      </div>
    </RestaurantCard>
  );
}

// edit this to fit your needs
const Body = styled.div`
  padding: 1px 14px;
  width: 375px;
  height: 667px;
  background-color: #fafafa;

  font-family: "Avenir Next";

  box-shadow: 0 5px 22px 2px rgba(128, 128, 128, 0.17);
  overflow-y: auto;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 15px;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 22px;
    font-weight: 700;
  }
`;

const RedeemOffer = styled.div`
  width: 304px;
  color: #9f9c9c;
  font-family: "Avenir Next";
  font-size: 15px;
  font-weight: 700;
  margin: 0 auto;
  line-height: 1.6;
  margin-bottom: 40px;
`;

const Datebtn = styled.div`
  width: 284px;
  height: 32px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-color: #d0011b;

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 15px;
  font-weight: 700;
  line-height: 34px;
  text-align: center;
  margin: 0 auto;
  margin-bottom: 10px;
`;

const ExpiredBtn = styled.div`
  width: 284px;
  height: 51px;
  margin: 40px auto 20px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 700;
  line-height: 53px;
  text-align: center;
`;

const restaurantData = [
  {
    img: require("../assets/black-irish-bar.png"),
    title: "Black Irish Bar",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$100",
    open: "8:00PM - 10:00PM"
  },
  {
    img: require("../assets/retro-kitchen-and-bar.png"),
    title: "Retro Bar",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$400",
    open: "All Day"
  }
];

const ViewPurchase = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 60 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <RedeemOffer>
        To redeem your offer click on the redeem button. All split purchases can
        only be redeemed by the organizer.
      </RedeemOffer>

      <Datebtn>Date: Mon, January 11th 2018 </Datebtn>
      <Restaurant {...restaurantData[0]} />

      <Datebtn style={{ marginTop: 20 }}>Date: Fri, March 4th 2018 </Datebtn>
      <Restaurant {...restaurantData[1]} />

      <ExpiredBtn>View Expired Offers</ExpiredBtn>
    </Body>
  );
};

export default ViewPurchase;
