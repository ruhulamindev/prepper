import React from "react";
import styled from "styled-components";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .location-main {
    text-align: center;
    margin-top: 40px;

    img {
      margin-bottom: 30px;
    }

    h1 {
      color: #000000;
      font-family: "Avenir Next";
      font-size: 33px;
      font-weight: bolder;
      margin-bottom: 60px;
    }
    p {
      color: #9f9c9c;
      font-family: "Avenir Next";
      font-size: 22px;
      font-weight: 600;
      padding: 17px 35px;
      margin-bottom: 50px;
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;
`;

const Close = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 50%;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  padding: 1px 10px;
  color: #ffffff;
  font-size: 30px;
  font-weight: 400;
  text-transform: uppercase;
`;

const PushButton = styled.div`
  width: 304px;
  height: 57px;
  margin: 0 auto;
  margin-top: 20px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 22px;
  font-weight: 700;
  text-align: center;
  line-height: 59px;
`;

const EnableLocation = () => {
  return (
    <Body>
      <Header>
        <Close>&times;</Close>
      </Header>

      <div className="location-main">
        <img src={require("../assets/location-icon.png")} alt="location icon" />
        <h1>See Deals Nearby!</h1>
        <p>Enable location services to see deals nearby.</p>
        <PushButton>Enable Location Services</PushButton>
      </div>
    </Body>
  );
};

export default EnableLocation;
