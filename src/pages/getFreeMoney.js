import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 22px;
    font-weight: 700;
    margin: 0;
  }
`;

const Body = styled.div`
  width: 375px;
  height: 680px;
  background-color: #fafafa;
  padding: 16px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
`;

const FreeIntro = styled.div`
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 16px;
  font-weight: 500;
  padding: 5px 20px;
`;

const Main = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  border-radius: 10px;
  background-color: #ffffff;
  padding: 21px 16px;

  .earn-block {
    margin-bottom: 25px;

    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 500;
  }

  .earn-btn {
    width: 116px;
    height: 32px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 100px;
    background-color: #d0011b;

    color: #ffffff;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
    line-height: 34px;
    text-align: center;
    margin-bottom: 14px;
  }
  .earn-title {
    font-weight: 700;
    margin-bottom: 5px;
  }

  .earn-title,
  .earn-details {
    padding-left: 15px;
  }
`;

const ReferrelCode = styled.div`
  text-align: center;
  margin: 50px 0 21px;

  .title {
    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
    text-transform: uppercase;
  }

  .input-wrapper {
    border-radius: 100px;
    border: 2px solid #707070;
    background-color: #ffffff;

    display: flex;
    justify-content: space-between;
    overflow: hidden;
    align-items: center;
    padding: 3px;


    input {
      border: 0;
      color: #4a494a;
      font-family: "Avenir Next";
      font-size: 15px;
      font-weight: 700;
      padding-left: 18px;
    }

    div {
      width: 88px;
      height: 39px;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 100px;
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      text-align: center;
      line-height: 41px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 18px;
      font-weight: 700;
    }
  }
`;

const GetFreeMoney = () => {
  return (
    <Body>
      <Header>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>Get Free Money</h1>
        <span>&nbsp;</span>
      </Header>

      <FreeIntro>
        There are two great ways to help Pepper grow and make money in the
        process! Share your referral code with your friends, blog and followers
        and make money the easy way!
      </FreeIntro>

      <Main>
        <div className="earn-block">
          <div className="earn-btn">Earn $5.00</div>
          <div className="earn-title">Invite Your Friends to Pepper</div>
          <div className="earn-details">
            For every friend who signs up with your referral code, you will
            receive $5.00 to spend on Pepper and they will also receive $5.00.
          </div>
        </div>

        <div className="earn-block">
          <div className="earn-btn">Earn $100.00</div>
          <div className="earn-title">Invite Businesses to Pepper</div>
          <div className="earn-details">
            For every business that signs up with your referral code, you will
            receive $100 to spend on Pepper.
          </div>
        </div>

        {/* referrel code and input */}
        <ReferrelCode>
          <div className="title">YOUR REFERRAL CODE</div>
          <div className="input-wrapper">
            <input type="text" placeholder="Britx22" />
            <div>share</div>
          </div>
        </ReferrelCode>
      </Main>
    </Body>
  );
};

export default GetFreeMoney;
