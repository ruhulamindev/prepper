import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  width: 375px;
  height: 667px;
  background-color: #fafafa;
  font-family: "Avenir Next";
  padding: 15px 14px;

  display: flex;
  flex-flow: column;


  /* just for the browser */
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
  padding-bottom: 20px;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 30px;
    font-weight: bolder;

    color: #d0011b;
    font-weight: 700;
  }
`;

const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const MainContent = styled(Card)`
  width: 100%;
  padding: 17px 30px;
  margin-top: 40px;
  height: 100%;

  h1 {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
    text-align: center;
    margin-bottom: 30px;
  }
`;

const PaymentWrapper = styled.div`
  margin: 0 auto;

  & > * {
    margin-bottom: 40px;
  }
`;

const PaymentMethod = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: bold;

  .paymentTitle {
    font-size: 16px;
  }
  .paymetnDetails {
    font-size: 12px;
  }
`;

const PaymentButton = styled.div`
  height: 38px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
  padding: 0 45px;

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 20px;
  font-weight: 700;
  line-height: 40px;
  text-align: center;
`;

const AddPaymentButton = styled.div`
  width: 148px;
  height: 39px;
  margin: 0 auto;
  text-align: center;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-color: #4a494a;

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 12px;
  font-weight: bolder;
  line-height: 41px;
`;

const SelectPaymentMethod = () => {
  return (
    <Body>
      <Header>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <MainContent>
        <h1>Select Payment Method</h1>
        <PaymentWrapper>
          <PaymentCard title="Visa" details="Card Ending 0668" />
          <PaymentCard title="Master Card" details="Card Ending 0668" />
          <PaymentCard title="Visa" details="Card Ending 0668" />
        </PaymentWrapper>

        <AddPaymentButton style={{ marginTop: 60 }}>
          Add Payment Method
        </AddPaymentButton>
      </MainContent>
    </Body>
  );
};

export default SelectPaymentMethod;

function PaymentCard({ title, details }) {
  return (
    <PaymentMethod>
      <div className="paymentName">
        <div className="paymentTitle">{title}</div>
        <div className="paymetnDetails">{details}</div>
      </div>
      <PaymentButton>Select</PaymentButton>
    </PaymentMethod>
  );
}
