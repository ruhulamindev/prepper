import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .cvcinfo {
    &-main {
      text-align: center;
      width: 80%;
      margin: 0 auto;
      margin-top: 70px;
    }

    &-details {
      opacity: 0.5;
      color: #000000;
      font-family: "Avenir Next";
      font-size: 12px;
      font-weight: 700;
      margin-top: 10px;
    }

    &-button {
      width: 210px;
      margin: 0 auto;
      margin-top: 80px;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 50px;
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      height: 45px;
      line-height: 47px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 22px;
      font-weight: 700;
      text-align: center;
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
  }
`;

const CvcInfo = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 30 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>CVC Information</h1>
        <span>&nbsp;</span>
      </Header>

      <div className="cvcinfo-main">
        <img src={require("../assets/card-image.png")} alt="credit card" />
        <div className="cvcinfo-details">
          The CVC Number Is The Last Three Digits On The BACK Of Your Credit
          Card
        </div>
      </div>

      <div className="cvcinfo-button">Got it</div>
    </Body>
  );
};

export default CvcInfo;
