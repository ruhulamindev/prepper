import React from "react";
import styled from "styled-components";

const Body = styled.div`
  background-color: #fafafa;
  font-family: "Avenir Next";
  padding: 15px 14px;

  display: flex;
  flex-flow: column;

  /* just for the browser */
  width: 375px;
  overflow-y: auto;
  min-height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
  padding-bottom: 20px;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: center;

  h1 {
    font-size: 30px;
    font-weight: bolder;
    color: #d0011b;
    font-weight: 700;
  }
`;

const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const MainContent = styled(Card)`
  width: 100%;
  padding: 25px;
  margin-top: 40px;
  height: 100%;

  h1 {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
    text-align: center;
    margin-bottom: 30px;
  }
`;

const RestaurantDetails = {
  img: require("../assets/black-irish-bar.png"),
  title: "Black Irish Bar",
  twitter: "@blackirish",
  location: "901 King Street, Unit 432, Toronto",
  price: "$130",
  open: "8:00PM - 10:00PM"
};

const SummeryOfPayment = () => {
  return (
    <Body>
      <Header>
        <h1>pepper</h1>
      </Header>

      <MainContent>
        <Restaurant {...RestaurantDetails} />

        <PaymentFor />

        <PaymentSummery />
      </MainContent>
    </Body>
  );
};

export default SummeryOfPayment;

const PaymentSummeryWrapper = styled.div`
  .pm-summery-title {
    width: 183px;
    height: 27px;
    margin: 0 auto;
    margin-bottom: 16px;

    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
    text-align: center;

    padding-bottom: 4px;
    border-bottom: 2px solid #d0011b;
  }

  .pm-calculation {
    padding: 0 20px;

    & > * {
      display: flex;
      justify-content: space-between;

      opacity: 0.9;
      color: #4a4a4a;
      font-family: "Avenir Next";
      font-size: 18px;
      line-height: 35px;

      &:last-child {
        font-weight: 700;
      }

      &:not(:last-child) {
        margin-bottom: 14px;
      }
    }
  }

  .pm-pay-button {
    width: 296px;
    height: 50px;
    border-radius: 100px;

    margin-top: 20px;

    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
    line-height: 52px;
    text-align: center;
    color: #d0011b;

    &--colored {
      color: #ffffff;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
    }
  }
`;

function PaymentSummery() {
  return (
    <PaymentSummeryWrapper>
      <div className="pm-summery-title">Payment Summary</div>
      <div className="pm-calculation">
        <div>
          <div className="pm-calc-name">Price</div>
          <div>$100.00 </div>
        </div>
        <div>
          <div className="pm-calc-name">Credits</div>
          <div>($25.00)</div>
        </div>
        <div>
          <div className="pm-calc-name">Total</div>
          <div>$75.00</div>
        </div>
      </div>

      <div className="pm-pay-button pm-pay-button--colored">Pay</div>
      <div className="pm-pay-button">back</div>
    </PaymentSummeryWrapper>
  );
}

const PaymentForWrapper = styled.div`
  color: #000000;
  font-family: "Avenir Next";
  font-size: 12px;
  font-weight: 500;
  margin-bottom: 30px;

  .payment-for__title {
    color: #9f9c9c;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
    margin-bottom: 20px;
  }

  .payment-for__details {
    div {
      margin-bottom: 5px;
    }
  }
`;

function PaymentFor() {
  return (
    <PaymentForWrapper>
      <div className="payment-for__title">Confirm Payment for:</div>
      <div className="payment-for__details">
        <div>
          <b>Offer:</b> $130 for $100
        </div>
        <div>
          <b>Name:</b> Black Irish Bar
        </div>
        <div>
          <b>Location:</b> 12354 Younge Street,
        </div>
        <div>Toronto ON L6M 0R9</div>
        <div>
          <b>Valid From: </b> Thursday, August 17th 2018 (All Day)
        </div>
      </div>
    </PaymentForWrapper>
  );
}

// restaurant card
const RestaurantCard = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 43px;

  .restaurant-profile-wrapper {
    display: flex;
    align-items: center;

    .restaurant-profile {
      margin-left: 14px;

      h2 {
        font-size: 18px;
        font-weight: 700;
      }

      p {
        font-size: 13px;
        font-weight: 700;
        margin: 5px 0;
      }
    }
  }

  .restaurant-price {
    text-align: right;

    .price {
      color: #a2a2a2;
      font-size: 12px;
      font-weight: 600;
      line-height: 17px;
      margin-bottom: 5px;

      b {
        color: #000000;
        font-size: 20px;
        font-weight: 700;
      }

      p {
        margin-top: 5px;
      }
    }

    .duration-button {
      width: 89px;
      height: 22px;
      border-radius: 12px;
      background-color: #9f9c9c;
      padding: 0 8px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 8px;
      font-weight: 500;
      text-transform: uppercase;
      line-height: 24px;
    }
  }
`;

function Restaurant(props) {
  return (
    <RestaurantCard>
      <div className="restaurant-intro">
        <div className="restaurant-profile-wrapper">
          <img src={props.img} alt={props.title} />
          <div className="restaurant-profile">
            <h2>{props.title}</h2>
            <p>{props.twitter}</p>
          </div>
        </div>
      </div>

      <div className="restaurant-price">
        <div className="price">
          <b>{props.price}</b>
          <p>for $100</p>
        </div>

        <div className="duration-button">all day</div>
      </div>
    </RestaurantCard>
  );
}
