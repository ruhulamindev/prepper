import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import styled from "styled-components";

const Body = styled.div`
  width: 375px;
  height: 667px;
  background-color: #fafafa;
  box-shadow: 0 5px 22px 2px rgba(128, 128, 128, 0.17);
  padding: 1px 0;
  overflow-y: auto;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 48px;
  padding-left: 20px;

  h1 {
    font-size: 30px;
    font-weight: 700;
    margin: 0;
  }
`;

const RestaurantProfile = styled.div`
  color: #000000;
  font-family: "Avenir Next";
  font-size: 13px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 10px;

  img {
    width: 78px;
    height: 78px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 50%;
    margin-bottom: 6px;
  }
  .title {
    font-size: 20px;
  }
`;

const RestaurantDetails = styled.div`
  color: #000000;
  font-family: "Avenir Next";
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
  width: 318px;
  margin: 0 auto;
  margin-bottom: 30px;
`;

const Gallery = styled.div`
  margin-bottom: 25px;
  width: 100%;

  .title {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
    padding-left: 20px;
    margin-bottom: 10px;
  }

  .images {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;

    img {
      margin: 2px;
      margin-top: 3px;
      width: calc(32% - 2px);
    }
  }
`;

const ViewMenu = () => {
  return (
    <Body>
      <Header>
        <FontAwesomeIcon icon={faChevronLeft} />
        <span>&nbsp;</span>
      </Header>

      <RestaurantProfile>
        <img src={require("../assets/black-irish-bar.png")} alt="some alt" />
        <div className="title">Black Irish Bar</div>
        <div className="twitter">@blackirish</div>
      </RestaurantProfile>

      <RestaurantDetails>
        <div>
          <b>ADDRESS: 901 King Street, Unit 432, Toronto</b>
        </div>
        <div>
          <b>PHONE:</b> 905-599-4961
        </div>
        <div>Rated #1 Restaurant in Toronto. Call us to find out more</div>
      </RestaurantDetails>

      <Gallery>
        <div className="title">Menu</div>
        <div className="images">
          <img src={require("../assets/image-2.png")} alt="" />
          <img src={require("../assets/image-3.png")} alt="" />
          <img src={require("../assets/image-4.png")} alt="" />
        </div>
      </Gallery>

      <Gallery>
        <div className="title">Menu</div>
        <div className="images">
          {[...new Array(4)].map(_ => (
            <React.Fragment key={Math.random()}>
              <img src={require("../assets/image-2.png")} alt="" />
              <img src={require("../assets/image-3.png")} alt="" />
              <img src={require("../assets/image-4.png")} alt="" />
            </React.Fragment>
          ))}
        </div>
      </Gallery>
    </Body>
  );
};

export default ViewMenu;
