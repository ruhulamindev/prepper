import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;
  overflow-y: auto;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .addcard-main {
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;

    .addcard-input-wrapper {
      background-color: #efefef;
      width: 100%;
      border-bottom: 3px solid #d0011b;
      padding: 3px;

      .addcard-label {
        opacity: 0.5;
        color: #000000;
        font-family: "Avenir Next";
        font-size: 15px;
        font-weight: 700;
      }

      .addcard-cvv {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;

        img {
          width: 15px;
          height: 15px;
        }
      }

      &:not(:last-child) {
        margin-bottom: 10px;
      }

      input {
        width: 100%;
        border: 0;
        background: none;
      }
    }
  }

  .accept-terms-wrapper {
    text-align: center;
    margin-top: 170px;
    margin-bottom: 15px;

    color: #000000;
    opacity: .5;
    font-family: "Avenir Next";
    font-size: 12px;
    font-weight: 700;
    display: flex;
    align-items: center;
    justify-content: center;

    input{
      margin-right: 10px;
    }
  }

  .addcard-save-button {
    margin: 0 auto;
    margin-top: 13px;
    width: 210px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 50px;
    background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
    height: 46px;
    line-height: 46px;

    color: #ffffff;
    font-family: "Avenir Next";
    font-size: 22px;
    font-weight: 700;
    text-align: center;
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 25px;
    font-weight: 700;
  }
`;

const AddCard = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 30 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>Sign Up</h1>
        <span>&nbsp;</span>
      </Header>

      <div className="addcard-main">
        <div className="addcard-input-wrapper">
          <div className="addcard-label">Card Number</div>
          <input type="text" />
        </div>
        <div className="addcard-input-wrapper">
          <div className="addcard-label">Name on Card</div>
          <input type="text" />
        </div>
        <div style={{ width: "48%" }} className="addcard-input-wrapper">
          <div className="addcard-label">Exp. Date (MM/YY)</div>
          <input type="text" />
        </div>
        <div style={{ width: "48%" }} className="addcard-input-wrapper">
          <div className="addcard-label addcard-cvv">
            <span>CVV</span>
            <img src={require("../assets/info.png")} alt="info" />
          </div>
          <input type="text" />
        </div>
        <div className="addcard-input-wrapper">
          <div className="addcard-label">Country</div>
          <input type="text" />
        </div>
        <div className="addcard-input-wrapper">
          <div className="addcard-label">Postal Code</div>
          <input type="text" />
        </div>
      </div>
      {/* forms finished */}

      <div className="accept-terms-wrapper">
        <input type="checkbox" /> Accept Terms & Conditions
      </div>
      <div className="addcard-save-button">Save</div>
    </Body>
  );
};

export default AddCard;
