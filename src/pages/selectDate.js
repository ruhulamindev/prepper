import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const SpendCard = styled.div`
  width: 126px;
  height: 36px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-color: #ffffff;

  line-height: 38px;
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 15px;
  font-weight: 700;
  text-align: center;
`;

const OfferDealBtn = styled.div`
  width: 296px;
  height: 50px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 700;
  text-align: center;
  line-height: 52px;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 30px;
    font-weight: 700;
  }
`;

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 25px;
  overflow-y: auto;

  width: 375px;
  height: 667px;
  background-color: #fafafa;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .spend-selected {
    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .spend-date {
    margin: 25px auto;

    h2 {
      color: #4a494a;
      font-family: "Avenir Next";
      font-size: 25px;
      font-weight: 700;
      margin-bottom: 14px;
    }

    .calender {
      color: #4a494a;
      font-family: "Avenir Next";
      font-size: 12px;
      font-weight: 700;
      margin-left: 20px;

      &-label{
        margin-bottom: 5px;
      }
    }
  }
`;

const SelectDate = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 30 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <div className="spend-selected">
        <p>Desired Spend Selected</p>
        <SpendCard>$100</SpendCard>
      </div>

      <div className="spend-date">
        <h2>What day do you want a deal for?</h2>

        <div className="calender">
          <div className="calender-label">Select Date: August 17th, 2018</div>
          <img
            src={require("../assets/calender.png")}
            alt="calender"
            className="calender-img"
          />
        </div>
      </div>

      <OfferDealBtn>Get Exclusive Offers</OfferDealBtn>
    </Body>
  );
};

export default SelectDate;
