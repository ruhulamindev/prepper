import React from "react";
import styled from "styled-components";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .notification-main {
    text-align: center;
    margin-top: 40px;

    img {
      margin-bottom: 17px;
    }

    h1 {
      color: #000000;
      font-family: "Avenir Next";
      font-size: 33px;
      font-weight: bolder;
    }
    p {
      color: #9f9c9c;
      font-family: "Avenir Next";
      font-size: 22px;
      font-weight: 500;
      padding: 17px 35px;
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;
`;

const Close = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 50%;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  padding: 1px 10px;
  color: #ffffff;
  font-size: 30px;
  font-weight: 400;
  text-transform: uppercase;
`;

const PushButton = styled.div`
  width: 304px;
  height: 57px;
  margin: 0 auto;
  margin-top: 20px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 22px;
  font-weight: 700;
  text-align: center;
  line-height: 59px;
`;

const Notification = () => {
  return (
    <Body>
      <Header>
        <Close>&times;</Close>
      </Header>

      <div className="notification-main">
        <img
          src={require("../assets/notification-icon.png")}
          alt="Notification icon"
        />
        <h1>Enable Push Notifications</h1>
        <p>
          Please enable your push notifications for special deals and to be
          reminded when your deals are about to expire.
        </p>

        <PushButton>Enable Push</PushButton>
      </div>
    </Body>
  );
};

export default Notification;
