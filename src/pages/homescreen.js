import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH, faQuestion } from "@fortawesome/free-solid-svg-icons";

// buttons on the footer
const FooterBtn = styled.div`
  width: 171px;
  height: 50px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-color: #ffffff;

  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 15px;
  font-weight: 700;
  line-height: 52px;
  text-align: center;
`;

// buttons above the profile card
const Exclusivebtn = styled.div`
  width: 311px;
  height: 50px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-color: #ffffff;

  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 20px;
  font-weight: 700;
  line-height: 52px;
  text-align: center;
`;

const Body = styled.div`
  width: 375px;
  height: 667px;
  background-image: linear-gradient(
    180deg,
    #d0011b 0%,
    #fc445b 70%,
    #d0011b 100%
  );
  padding: 1px 12px;
  position: relative;

  /* browser */
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  /* other styles */

  .help {
    width: 37px;
    height: 37px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    background-color: #ffffff;

    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 24px;
    font-weight: 700;

    border-radius: 50%;
    padding: 9px;

    position: absolute;
    top: 20px;
    right: 12px;
  }

  .mainHeader {
    text-align: center;
    margin-bottom: 40px;
    margin-top: 80px;

    h1 {
      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 44px;
      font-weight: 700;
    }
    h2 {
      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 28px;
      font-weight: 700;
    }
  }
  // header finish

  // main content styles
  .main-content {
    text-align: center;

    .title {
      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 20px;
      font-weight: 500;
      padding: 10px 20px;
    }

    .user-card {
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 10px;
      background-color: #ffffff;
      padding: 12px 15px;
      position: relative;
    }

    .user-profile {
      border-bottom: 1px solid #efefef;
      padding-bottom: 5px;

      &__pic {
        width: 81px;
        height: 81px;
      }
      &__name {
        color: #d0011b;
        font-family: "Avenir Next";
        font-size: 22px;
        font-weight: 700;
      }
    }

    .user-transaction {
      display: flex;
      align-items: center;
      justify-content: space-between;

      .transaction {
        width: 50%;
        padding-top: 18px;

        &:first-child {
          border-right: 1px solid #efefef;
        }

        &-title {
          color: #707070;
          font-family: "Avenir Next";
          font-size: 15px;
          font-weight: 700;
          text-transform: uppercase;
          margin-top: 5px;
        }

        &-amount {
          color: #d0011b;
          font-family: "Avenir Next";
          font-size: 25px;
          font-weight: 700;
          margin-top: 5px;
        }
      }
    }

    /* footer */
    footer {
      margin-top: 15px;
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
  }
`;

const Option = styled(FontAwesomeIcon)`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  background-color: #d3d3d3;
  border-radius: 50%;
  padding: 7px;
  box-sizing: content-box;

  color: #ffffff;
  font-size: 17px;
  font-weight: 400;

  position: absolute;
  top: 10px;
  right: 10px;
`;

const Homescreen = () => {
  return (
    <Body>
      <FontAwesomeIcon className="help" icon={faQuestion} />

      <header className="mainHeader">
        <h1>pepper</h1>
        <h2>save. eat. enjoy.</h2>
      </header>

      <main className="main-content">
        <div className="title">
          Earn $5.00 for every friend you refer to Pepper!
        </div>
        <Exclusivebtn style={{ margin: "20px auto" }}>
          Get Exclusive Offers
        </Exclusivebtn>

        <div className="user-card">
          <Option icon={faEllipsisH} />

          <div className="user-profile">
            <img
              src={require("../assets/signup-img.png")}
              alt="prfile pic"
              className="user-profile__pic"
            />
            <div className="user-profile__name">Britx22</div>
          </div>

          <div className="user-transaction">
            <div className="transaction">
              <div className="transaction-title">YOUR SAVINGS</div>
              <div className="transaction-amount">$4,523</div>
            </div>

            <div className="transaction">
              <div className="transaction-title">DEALS BOUGHT</div>
              <div className="transaction-amount">54</div>
            </div>
          </div>
        </div>

        <footer>
          <FooterBtn>View Purchases</FooterBtn>
          <FooterBtn>Invite Friends</FooterBtn>
        </footer>
      </main>
    </Body>
  );
};

export default Homescreen;
