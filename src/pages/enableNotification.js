import React from "react";
import styled from "styled-components";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

// edit this to fit your needs
const Body = styled.div`
  width: 375px;
  height: 667px;
  background-color: #fafafa;
  display: flex;

  font-family: "Avenir Next" ;
`;

const NotificationStyle = styled.div`
  width: 100%;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  margin: 15px 14px 0;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  padding: 0 8px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 20px;
    font-weight: 700;
    margin: 0;
  }
`;

const Main = styled.main`
  width: 100%;
  margin-top: 40px;
  text-align: center;

  h2 {
    color: #4a494a;
    font-size: 14px;
    font-weight: 700;
    margin-bottom: 15px;
  }
`;

const Button = styled.div`
  width: 285px;
  height: auto;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
  text-align: center;
  padding: 8px 0;
  display: inline-block;
`;

const EnableNotification = () => {
  return (
    <Body>
      <NotificationStyle>
        <Header>
          <FontAwesomeIcon icon={faChevronLeft} />
          <h1>Notifications</h1>
          <span>&nbsp;</span>
        </Header>

        <Main>
          <h2>Push notifications are disabled for Pepper</h2>
          <Button>Enable Notifications</Button>
        </Main>
      </NotificationStyle>
    </Body>
  );
};

export default EnableNotification;
