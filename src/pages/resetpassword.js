import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

// edit this to fit your needs
const Body = styled.div`
  padding: 1px 32px;
  width: 375px;
  height: 667px;
  background-color: #ffffff;

  font-family: "Avenir Next";
  /* just for the browser */
  box-shadow: 0 5px 22px 2px rgba(128, 128, 128, 0.17);
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  h1 {
    font-size: 22px;
    font-weight: 700;
    margin: 0;
  }
`;

const Main = styled.main`
  width: 100%;
  margin-top: 50px;
  text-align: center;

  h1 {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 28px;
    font-weight: 700;
    margin-bottom: 25px;
  }

  h2 {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 20px;
    font-weight: 700;
  }

  & > * {
    margin-bottom: 25px;
  }
`;

const Button = styled.div`
  width: 311px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 11px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
  display: inline-block;
  color: #ffffff;
  font-size: 22px;
  font-weight: 700;
  padding: 16px 0;
`;

const Input = styled.input`
  width: 311px;
  padding: 21px;
  border: none;
  border-radius: 11px;
  background-color: #efefef;
  color: #000000;
  font-size: 17px;
  font-weight: 700;
  text-align: center;

  &:focus {
    border: none;
    outline: none;
  }
`;

const ResetPassWord = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 60 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <Main>
        <h1>Forgot password?</h1>
        <h2>
          We just need your registerd email address to send your password reset
        </h2>

        <Input placeholder="brit@spears.com" />

        <Button>Reset Password</Button>
      </Main>
    </Body>
  );
};

export default ResetPassWord;
