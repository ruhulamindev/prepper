import React from "react";
import styled, { css } from "styled-components";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;
  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  background: url(${require("../assets/bg1.png")}),
    url(${require("../assets/bg2.png")});
  background-repeat: no-repeat;
  background-position: bottom left, bottom right;
  text-align: center;

  .fs-main-title {
    margin-bottom: 52px;
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 66px;
    font-weight: 700;
    text-align: center;
    margin: 0 auto;
    margin-top: 80px;
    margin-bottom: 20px;
  }

  .fs-secondary-title {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 35px;
    font-weight: 700;
    margin-bottom: 60px;
  }

  .fs-available {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
    line-height: 22px;
    margin-bottom: 20px;

    h3 {
      font-size: 22px;
    }
  }
`;

const LoginButton = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 11px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 11px;
  padding: 18px 0;

  font-family: "Avenir Next";
  font-size: 22px;
  font-weight: 700;
  text-align: center;
  color: #d0011b;

  ${props =>
    props.colored
      ? css`
          border: 4px solid #d0011b;
          background-color: #ffffff;
          padding: 15px;
        `
      : css`
          background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
          color: #ffffff;
        `};
`;

const Dots = styled.ul`
  list-style: none;
  margin-top: 70px;

  li {
    display: inline-block;
    width: 8px;
    height: 8px;
    border-radius: 50%;
    margin-left: 20px;
    background: #9f9c9c;
  }
  .active {
    background: #d0011b;
  }
`;

const Term = styled.div`
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 11px;
  font-weight: 700;
  text-align: center;
  padding: 10px 60px;
  margin-top: 20px;
`;

const FirstScreen = () => {
  return (
    <Body>
      <h1 className="fs-main-title">pepper</h1>
      <h2 className="fs-secondary-title">save. eat. enjoy.</h2>
      <div className="fs-available">
        <h3>Available Only in Toronto.</h3>
        <h4>(Other cities coming soon)</h4>
      </div>
      <LoginButton style={{ marginBottom: 19 }}>Login</LoginButton>
      <LoginButton colored>Sign Up</LoginButton>
      <Dots>
        <li className="active" />
        <li />
        <li />
        <li />
      </Dots>
      <Term>
        By signing up you agree with Pepper’s Terms of Service and Privacy
        Policy
      </Term>
    </Body>
  );
};

export default FirstScreen;
