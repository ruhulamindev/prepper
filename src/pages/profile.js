import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 22px;
    font-weight: 700;
    margin: 0;
  }
`;

const Body = styled.div`
  background-color: #fafafa;
  padding: 0 12px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
`;

const ProfileCard = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 10px;
  background-color: #ffffff;
  padding: 9px 18px;
  position: relative;
  text-align: center;

  .edit {
    position: absolute;
    top: 9px;
    right: 18px;

    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 600;
  }

  img {
    width: 82px;
    height: 82px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 50%;
    margin-bottom: 10px;
  }

  .user-name {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 24px;
    font-weight: 700;
  }
  .user-twitter {
    font-family: "Avenir Next";
    font-weight: 700;
    font-size: 18px;
  }
`;

const Options = styled.div`
  text-align: center;

  & > * {
    margin-top: 15px;
  }

  .credits {
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 700;
  }

  .invite-btn {
    width: 200px;
    height: 51px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 100px;
    background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
    margin: 0 auto;
    margin-top: 15px;

    color: #ffffff;
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 700;
    line-height: 53px;
    text-align: center;
  }
  .title {
    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 600;
    margin-top: 30px;
  }

  .option-tile {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 9px 16px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 7px;
    background-color: #ffffff;

    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 700;
  }
`;

const Footer = styled.div`
  width: 183px;
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 600;
  margin: 30px auto 20px;

  span {
    font-family: "Apple Color Emoji";
    font-weight: 400;
  }
`;

const Profile = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 60 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <ProfileCard>
        <div className="edit">edit</div>
        <img
          src={require("../assets/signup-img.png")}
          alt="profile pic"
          className="profile-pic"
        />
        <div className="user-name">Britney Spears</div>
        <div className="user-twitter">@Britx22</div>
      </ProfileCard>

      <Options>
        <div className="credits">Your Credits $17,100</div>
        <div className="invite-btn">Invite Friends</div>
        <div className="title">Options</div>
        {[
          ["Birthday", "11/11/86"],
          ["Email", "brit@brit.com"],
          ["Notification Settings", "edit"],
          ["Payment Method", "edit"],
          ["Password", "edit"],
          ["Terms of Service", ""],
          ["Privacy Policy", ""],
          ["Log Out", ""]
        ].map(data => (
          <div key={data[0]} className="option-tile">
            <span>{data[0]}</span>
            <span>{data[1]}</span>
          </div>
        ))}
      </Options>

      <Footer>
        Pepper v.1.0.0 mmm{" "}
        <span aria-label="love" role="img">
          👅
        </span>{" "}
        Made in Toronto
      </Footer>
    </Body>
  );
};

export default Profile;
