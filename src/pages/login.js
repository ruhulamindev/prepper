import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  background: url(${require("../assets/bg1.png")}),
    url(${require("../assets/bg2.png")});

  background-repeat: no-repeat;
  background-position: bottom left, bottom right;

  .login-main {
    text-align: center;

    &-title {
      color: #000000;
      font-family: "Avenir Next";
      font-size: 28px;
      font-weight: 700;
      margin-bottom: 52px;
    }

    form {
      input {
        border-radius: 11px;
        background-color: #efefef;
        border: none;

        color: #000000;
        font-family: "Avenir Next";
        font-size: 17px;
        font-weight: 700;

        padding: 20px;
        width: 100%;
        text-align: center;

        &:not(:last-child) {
          margin-bottom: 20px;
        }
      }
    }

    &-button {
      margin-top: 47px;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 11px;
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      height: 62px;
      line-height: 64px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 22px;
      font-weight: 700;
    }
    &-forgot-password {
      margin-top: 32px;
      color: #9f9c9c;
      font-family: "Avenir Next";
      font-size: 17px;
      font-weight: 700;
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 25px;
    font-weight: 700;
  }
`;

const Login = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 80 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <div className="login-main">
        <div className="login-main-title">It’s so good to see you again!</div>
        <form>
          <input placeholder="brit@spears.com" type="text" />
          <input placeholder="password" type="password" />

          <div className="login-main-button">Login</div>
          <div className="login-main-forgot-password">Forgot Password</div>
        </form>
      </div>
    </Body>
  );
};

export default Login;
