import React from "react";
import styled from "styled-components";

const Body = styled.div`
  padding: 1px 14px;
  background-color: #fafafa;

  font-family: "Avenir Next" ;

  /* just for the browser */
  height: 667px;
  width: 380px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
  padding-bottom: 20px;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 20px;
    font-weight: 700;
    margin: 0;

    color: #d0011b;
    font-size: 30px;
    font-weight: 700;
  }
`;

const Close = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 50%;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  padding: 1px 10px;
  color: #ffffff;
  font-size: 30px;
  font-weight: 400;
  text-transform: uppercase;
`;

// basic card component:
const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const LocationStyle = styled(Card)`
  margin-top: 35px;
  padding: 15px 21px;
`;

const LocationItem = styled.div`
  color: #4a494a;
  font-size: 13px;
  line-height: 30px;

  display: flex;
  justify-content: space-between;

  &:not(:last-child) {
    margin-bottom: 10px;
  }

  & span:last-child {
    color: #d0011b;
    font-size: 13px;
    font-weight: 700;
  }
`;

const TimeOfferPickerStyle = styled(Card)`
  margin: 14px 0;
  padding: 12px 33px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    color: #9f9c9c;
    font-size: 12px;
    font-weight: 700;
  }
`;

const FilterStyle = styled(Card)`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
  height: 40px;

  & div:first-child {
    border-radius: 100px;
    padding: 0 16px;
    background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
    font-size: 15px;
    font-weight: 700;
    line-height: 41px;
    color: #ffffff;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  }

  & div:last-child {
    padding: 10px;
    font-size: 15px;
    font-weight: 700;
    display: flex;
    align-items: center;

    i {
      font-size: 20px;
      opacity: 0.5;
      margin-left: 10px;
    }
  }
`;

const ListOfDealsEvening = () => {
  return (
    <Body>
      <Header>
        <Close>&times;</Close>
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <LocationCard />

      <TimeOfferPicker active="Evening" />

      <Filter />

      <MainContent />
    </Body>
  );
};

export default ListOfDealsEvening;

function LocationCard() {
  return (
    <LocationStyle>
      <LocationItem>
        <span>
          <b>Showing:</b> Nearby
        </span>
        <span>Change Location</span>
      </LocationItem>

      <LocationItem>
        <span>
          <b>Date:</b> Tues, Aug 17th 2018
        </span>
        <span>Change Date</span>
      </LocationItem>

      <LocationItem>
        <span>
          <b>Pre-Payment:</b> $100
        </span>
        <span>Change Pre-Payment</span>
      </LocationItem>
    </LocationStyle>
  );
}

function TimeOfferPicker({ active }) {
  const activeStyle = {
    color: "#d0011b",
    borderBottom: "2px solid #d0011b",
    paddingBottom: 2
  };
  return (
    <TimeOfferPickerStyle>
      {["All Day", "All", "Morning", "Afternoon", "Evening"].map(time => (
        <span key={time} style={active === time ? activeStyle : {}}>
          {time}
        </span>
      ))}
    </TimeOfferPickerStyle>
  );
}

function Filter() {
  return (
    <FilterStyle>
      <div>Tues, Aug 17th, 2018 Offers</div>
      <div>
        <span>Filter</span>
        <i className="fa fa-spotify" />
      </div>
    </FilterStyle>
  );
}

const MainContentStyle = styled.div`
  h3 {
    color: #4a494a;
    font-size: 12px;
    font-weight: 500;
    text-align: right;
    margin-bottom: 15px;
  }

  & > div:not(:last-child) {
    margin-bottom: 8px;
  }
`;

const restaurantData = [
  {
    img: require("../assets/black-irish-bar.png"),
    title: "Black Irish Bar",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$100",
    open: "8:00PM - 10:00PM"
  },
  {
    img: require("../assets/the-plank.png"),
    title: "Plank Bar & Grill",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$200",
    open: "8:00PM - 10:00PM"
  },
  {
    img: require("../assets/hooty.png"),
    title: "Black Irish Bar",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$300",
    open: "8:00PM - 10:00PM"
  },
  {
    img: require("../assets/retro-kitchen-and-bar.png"),
    title: "Retro Bar",
    twitter: "@blackirish",
    location: "901 King Street, Unit 432, Toronto",
    price: "$400",
    open: "8:00PM - 10:00PM"
  }
];

function MainContent() {
  return (
    <MainContentStyle>
      <h3>Listing by Nearest Restaurant</h3>
      {restaurantData.map(res => <Restaurant key={res.title} {...res} />)}
    </MainContentStyle>
  );
}

const RestaurantCard = styled(Card)`
  padding: 11px 18px;
  display: flex;
  justify-content: space-between;

  .restaurant-profile-wrapper {
    display: flex;
    align-items: center;

    .restaurant-profile {
      margin-left: 14px;

      h2 {
        font-size: 18px;
        font-weight: 700;
      }

      .button {
        width: 77px;
        background-color: #4a494a;
        border-radius: 50px;
        color: white;
        font-size: 13px;
        text-align: center;
        padding: 3px 0px;
      }

      p {
        font-size: 13px;
        font-weight: 700;
        margin: 5px 0;
      }
    }
  }

  .restaurant-location {
    margin-top: 9px;
    font-size: 11px;
    font-weight: 500;

    p {
      line-height: 22px;
      margin-top: 5px;
      font-weight: 400;
    }
  }

  .restaurant-price {
    text-align: right;

    .price {
      color: #a2a2a2;
      font-size: 12px;
      font-weight: 600;
      line-height: 17px;
      margin-bottom: 10px;

      b {
        color: #000000;
        font-size: 20px;
        font-weight: 700;
      }

      p {
        margin-top: 5px;
      }
    }

    .select-button-wrapper {
      width: 98px;
      border-radius: 12px;
      background-color: #9f9c9c;
      text-align: center;
      margin-top: 20px;

      p {
        font-size: 8px;
        font-weight: 500;
        text-transform: uppercase;
        padding: 3px;
        color: #ffffff;
      }

      .select-button {
        border-radius: 14px;
        background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
        padding: 9px 0;
        font-size: 18px;
        font-weight: 700;
        color: white;
      }
    }
  }
`;

function Restaurant(props) {
  return (
    <RestaurantCard>
      <div className="restaurant-intro">
        <div className="restaurant-profile-wrapper">
          <img src={props.img} alt={props.title} />
          <div className="restaurant-profile">
            <h2>{props.title}</h2>
            <p>{props.twitter}</p>
            <div className="button">profile</div>
          </div>
        </div>

        <div className="restaurant-location">
          <b>{props.location}</b>
          <p>more details…</p>
        </div>
      </div>

      <div className="restaurant-price">
        <div className="price">
          <b>{props.price}</b>
          <p>for $100</p>
        </div>
        <div className="select-button-wrapper">
          <p>{props.open}</p>
          <div className="select-button">SELECT</div>
        </div>
      </div>
    </RestaurantCard>
  );
}
