import React from "react";
import styled from "styled-components";

const Body = styled.div`
  background-color: #fafafa;
  padding: 0 40px;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
`;

const Header = styled.div`
  height: 40px;
  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 30px;
  font-weight: 700;
  line-height: 42px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 60px;
`;

const Main = styled.div`
  h1 {
    color: #4a494a;
    font-family: "Avenir Next";
    font-size: 25px;
    font-weight: 700;
    line-height: 22px;

    margin-bottom: 20px;
  }
`;

const MoneyBalls = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-flow: row wrap;
  margin: 0 auto;

  div {
    width: 70px;
    height: 50px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 100px;
    background-image: linear-gradient(
      180deg,
      #d0011b 0%,
      #e8263e 67%,
      #fc445b 100%
    );

    color: #ffffff;
    font-family: "Avenir Next";
    font-size: 18px;
    font-weight: 700;
    line-height: 52px;
    text-align: center;
    margin: 5px 0;
  }
`;

const CustomInput = styled.input`
  width: 100%;
  margin: 20px 0;
  padding: 14px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 7px;
  background-color: #ffffff;
  border: 0;

  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 16px;
  font-weight: 600;
`;

const NextStepBtn = styled.div`
  height: 50px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 700;
  line-height: 52px;
  text-align: center;
  margin-bottom: 20px;
`;

const Tip = styled.div`
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 500;
  line-height: 22px;
  padding: 5px;
`;

const Footer = styled.div`
  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 22px;
  font-weight: 700;

  margin-top: 81px;
  text-align: center;
`;

const PrePayment = () => {
  return (
    <Body>
      <Header>pepper</Header>

      <Main>
        <h1>How much do you want to spend?</h1>
        <MoneyBalls>
          <div>$20</div>
          <div>$40</div>
          <div>$80</div>
          <div>$100</div>
          <div>$150</div>
          <div>$200</div>
        </MoneyBalls>

        <CustomInput placeholder="Enter Custom Amount" />

        <NextStepBtn>Next step: Select Date</NextStepBtn>

        <Tip>
          <b>Tip:</b> Typically restaurants will offer larger savings the more
          you pre-pay.
        </Tip>
      </Main>

      <Footer>cancel</Footer>
    </Body>
  );
};

export default PrePayment;
