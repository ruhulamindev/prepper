import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  width: 375px;
  height: 667px;
  background-color: #ffffff;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 37px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 20px;
  }
`;

const ThankYouNote = styled.div`
  width: 294px;
  margin: 50px auto;

  .quote {
    color: #000000;
    font-size: 20px;
    margin-bottom: 20px;
  }

  .author {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 700;
  }
`;

const Main = styled.div`
  text-align: center;
  margin-top: 70px;

  h1 {
    width: 250px;
    color: #000000;
    font-family: "Avenir Next";
    font-size: 33px;
    font-weight: 700;
    margin: 0 auto;
    margin-bottom: 10px;
  }
  input {
    border: 0;
    width: 311px;
    height: 62px;
    border-radius: 100px;
    background-color: #efefef;
    padding-left: 25px;

    color: #000000;
    font-family: "Avenir Next";
    font-size: 17px;
    font-weight: 700;
    margin-bottom: 25px;
  }
  .tip {
    width: 313px;
    color: #9f9c9c;
    font-family: "Avenir Next";
    font-size: 15px;
    font-weight: 500;
    margin: 0 auto;
  }
  .finishBtn {
    width: 217px;
    height: 57px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
    border-radius: 100px;
    background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

    color: #ffffff;
    font-family: "Avenir Next";
    font-size: 22px;
    font-weight: 700;
    line-height: 59px;
    margin: 30px auto;
  }
`;

const SignUpUserName = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 30 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <span>&nbsp;</span>
      </Header>

      <ThankYouNote>
        <div className="quote">
          “Thank you for joining Pepper. We hope you enjoy Pepper as much as we
          do.”
        </div>
        <div className="author">~ The Pepper Team</div>
      </ThankYouNote>

      <Main>
        <h1>Select a Username</h1>
        <input type="text" placeholder="Username" />
        <div className="tip">
          You agree to comply with the terms and conditions set forth in this
          Agreement.
        </div>
        <div className="finishBtn">Finish</div>
      </Main>
    </Body>
  );
};

export default SignUpUserName;
