import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  background-color: #fafafa;
  padding: 5px 23px;

  height: 667px;
  width: 375px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .about-main-content {
    color: #000000;
    font-family: "Avenir Next";
    font-size: 30px;
    font-weight: 700;
    line-height: 57px;
    margin-bottom: 93px;

    list-style: none;

    margin-left: 50px;

    li {
      position: relative;
    }

    li:before {
      content: "";
      width: 22px;
      height: 21px;
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      position: absolute;
      left: -38px;
      top: 15px;
    }
  }

  .about-footer {
    text-align: center;
    /* position: absolute; */
    bottom: 0;
    &-legal {
      color: #9f9c9c;
      font-family: "Avenir Next";
      font-size: 14px;
      font-weight: 700;
      margin-bottom: 13px;

      span:last-child {
        margin-left: 43px;
      }
    }

    &-title {
      border-bottom: 2px solid #d0011b;
      color: #000000;
      font-family: "Avenir Next";
      font-size: 30px;
      font-weight: 700;
      line-height: 55px;
      display: inline-block;
      margin-bottom: 28px;
    }

    .about-social-media {
      margin-bottom: 32px;
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 20px;
    font-weight: 700;
    margin: 0;
  }
`;

const About = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 80 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>About</h1>
        <span>&nbsp;</span>
      </Header>

      <ul className="about-main-content">
        <li>How to Save</li>
        <li>FAQs</li>
        <li>Get Help</li>
        <li>Rate Us</li>
      </ul>

      <footer className="about-footer">
        <div className="about-footer-title">follow us</div>
        <div className="about-social-media">
          <img src={require("../assets/social media.png")} alt="instagram" />
        </div>
        <div className="about-footer-legal">
          <span>privacey</span>
          <span>terms</span>
        </div>
      </footer>
    </Body>
  );
};

export default About;
