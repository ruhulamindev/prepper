import React from "react";
import styled from "styled-components";

// basic card component:
const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const RestaurantCard = styled.div`
  display: flex;
  justify-content: space-between;

  .restaurant-profile-wrapper {
    display: flex;
    align-items: center;

    .restaurant-profile {
      margin-left: 14px;

      h2 {
        font-size: 18px;
        font-weight: 700;
      }

      .button {
        width: 77px;
        background-color: #4a494a;
        border-radius: 50px;
        color: white;
        font-size: 13px;
        text-align: center;
        padding: 3px 0px;
      }

      p {
        font-size: 13px;
        font-weight: 700;
        margin: 5px 0;
      }
    }
  }

  .restaurant-location {
    margin-top: 9px;
    font-size: 11px;
    font-weight: 500;

    p {
      line-height: 22px;
      margin-top: 5px;
      font-weight: 400;
    }
  }

  .restaurant-price {
    text-align: right;

    .price {
      color: #a2a2a2;
      font-size: 12px;
      font-weight: 600;
      line-height: 17px;
      margin-bottom: 10px;

      b {
        color: #000000;
        font-size: 20px;
        font-weight: 700;
      }

      p {
        margin-top: 5px;
      }
    }

    .duration-button {
      width: 89px;
      height: 22px;
      border-radius: 12px;
      background-color: #9f9c9c;
      padding: 0 8px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 8px;
      font-weight: 500;
      text-transform: uppercase;
      line-height: 24px;
    }
  }
`;

function Restaurant({ style, ...props }) {
  return (
    <RestaurantCard style={{ ...style }}>
      <div className="restaurant-intro">
        <div className="restaurant-profile-wrapper">
          <img src={props.img} alt={props.title} />
          <div className="restaurant-profile">
            <h2>{props.title}</h2>
            <p>{props.twitter}</p>
            <div className="button">profile</div>
          </div>
        </div>

        <div className="restaurant-location">
          <b>{props.location}</b>
          <p>more details…</p>
        </div>
      </div>

      <div className="restaurant-price">
        <div className="price">
          <b>{props.price}</b>
          <p>for $100</p>
        </div>

        <div className="duration-button">{props.open}</div>
      </div>
    </RestaurantCard>
  );
}

const Body = styled.div`
  width: 375px;
  height: 667px;
  background-color: #fafafa;
  box-shadow: 0 5px 22px 2px rgba(128, 128, 128, 0.17);
  padding: 1px 14px;
`;

const Header = styled.div`
  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 30px;
  font-weight: 700;
  text-align: center;
  margin-top: 27px;
  margin-bottom: 68px;
`;

const Main = styled(Card)`
  padding: 11px 18px;
`;

const Promotion = styled.div`
  color: #000000;
  font-family: "Avenir Next";
  font-size: 15px;
  margin-bottom: 15px;
  font-weight: 700;

  .promotion-title {
    color: #9f9c9c;
    margin-bottom: 5px;
  }
`;

const FinePrint = styled.div`
  color: #4a494a;
  font-family: "Avenir Next";
  font-size: 12px;
  font-weight: 700;

  .fineprint-title {
    color: #9f9c9c;
    font-size: 22px;
    margin-bottom: 5px;
  }
`;

const VoucherValue = styled.div`
  color: #9f9c9c;
  font-family: "Avenir Next";
  font-size: 14px;
  font-weight: 700;
  margin: 15px 0;
  text-align: center;

  .voucher-title {
    color: #000000;
    font-size: 22px;
  }
`;

const PurchaseBtn = styled.div`
  width: 296px;
  height: 50px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 100px;
  background-image: linear-gradient(180deg, #23d2b8 0%, #29e6ca 100%);
  margin: 0 auto;

  color: #ffffff;
  font-family: "Avenir Next";
  font-size: 20px;
  font-weight: 700;
  line-height: 52px;
  text-align: center;
`;

const BackBtn = styled.div`
  color: #d0011b;
  font-family: "Avenir Next";
  font-size: 18px;
  font-weight: 700;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 20px;
`;

const PurchaseDeal = () => {
  return (
    <Body>
      <Header>pepper</Header>
      <Main>
        <Restaurant style={{ marginBottom: 45 }} {...RestaurantDetails} />
        <Promotion>
          <div className="promotion-title">Pre-paid promotion valid for:</div>
          <div className="promotion-date">
            Tursday, August 17th 2018 (All Day)
          </div>
        </Promotion>

        <FinePrint>
          <div className="fineprint-title">Fine Print</div>
          <div className="fineprint-details">
            Cannot be combined with any other coupon. If not used on selected
            date, promotional value expires and the value of the promo is at
            purchase price. Is only valid on Thursday, August 17th, 2018.
            Pre-paid promotion must be used on one bill.
          </div>
        </FinePrint>
        <VoucherValue>
          <div className="voucher-title">Voucher Value: $130</div>
          <div className="voucher-for">(for $100)</div>
        </VoucherValue>
        <PurchaseBtn>Purchase This Deal</PurchaseBtn>
        <BackBtn>back</BackBtn>
      </Main>
    </Body>
  );
};

export default PurchaseDeal;

var RestaurantDetails = {
  img: require("../assets/black-irish-bar.png"),
  title: "Black Irish Bar",
  twitter: "@blackirish",
  location: "901 King Street, Unit 432, Toronto",
  price: "$130",
  open: "all day"
};
