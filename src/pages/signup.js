import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faPlus } from "@fortawesome/free-solid-svg-icons";

const Body = styled.div`
  background-color: #ffffff;
  padding: 10px 19px;
  overflow-y: auto;

  width: 375px;
  height: 667px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);

  .signup-main {
    text-align: center;

    &-img-wrapper {
      position: relative;
      display: inline-block;
    }

    &-add {
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      position: absolute;
      top: 0;
      right: 0;
      border-radius: 50%;
      padding: 7px 9px;

      svg {
        color: #ffffff;
        font-family: "Avenir Next";
      }
    }

    &-title {
      color: #000000;
      font-family: "Avenir Next";
      font-size: 28px;
      font-weight: 700;
      margin-bottom: 52px;
    }

    form {
      .signup-input-wrapper {
        text-align: left;

        &:not(:last-child) {
          margin-bottom: 50px;
        }

        .signup-label {
          color: #9f9c9c;
          font-family: "Avenir Next";
          font-size: 17px;
          font-weight: 700;
          text-transform: uppercase;
          margin-bottom: 10px;
        }

        input {
          border: 0;
          border-bottom: 1px solid #efefef;
          width: 100%;
          padding: 10px 0;

          color: #4a494a;
          font-family: "Avenir Next";
          font-size: 18px;
          font-weight: 600;
        }
      }
    }

    &-button {
      margin-top: 47px;
      box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
      border-radius: 11px;
      background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
      height: 62px;
      line-height: 64px;

      color: #ffffff;
      font-family: "Avenir Next";
      font-size: 22px;
      font-weight: 700;
    }

    .signup-main-existing {
      margin-top: 19px;
      margin-bottom: 20px;
      font-family: "Avenir Next";
      font-size: 14px;
      text-transform: uppercase;
      color: #9f9c9c;
      font-weight: bolder;

      span {
        color: #d0011b;
        margin-left: 10px;
      }
    }
  }
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    color: #d0011b;
    font-family: "Avenir Next";
    font-size: 25px;
    font-weight: 700;
  }
`;

const SignUp = () => {
  return (
    <Body>
      <Header style={{ marginBottom: 30 }}>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>Sign Up</h1>
        <span>&nbsp;</span>
      </Header>

      <div className="signup-main">
        <div style={{ marginBottom: 20 }} className="signup-main-img-wrapper">
          <img
            src={require("../assets/signup-img.png")}
            className="signup-main-img"
            alt="Profile Pic"
          />
          <div className="signup-main-add">
            <FontAwesomeIcon icon={faPlus} />
          </div>
        </div>
        <form>
          <div className="signup-input-wrapper">
            <div className="signup-label">FULL NAME</div>
            <input placeholder="Britney Spears" type="text" />
          </div>

          <div className="signup-input-wrapper">
            <div className="signup-label">EMAIL</div>
            <input placeholder="brit@spears.com" type="text" />
          </div>

          <div className="signup-input-wrapper">
            <div className="signup-label">PASSWORD</div>
            <input placeholder=". . . . ." type="text" />
          </div>

          <div className="signup-main-button">Sign Up</div>
          <div className="signup-main-existing">
            ALREADY HAVE AN ACCOUNT? <span>SIGN IN</span>
          </div>
        </form>
      </div>
    </Body>
  );
};

export default SignUp;
