import React from "react";
import styled from "styled-components";

const Body = styled.div`
  padding: 1px 14px;
  background-color: #fafafa;
  font-family: "Avenir Next" ;

  /* just for the browser */
  height: 667px;
  width: 380px;
  box-shadow: 0 3px 12px rgba(128, 128, 128, 0.27);
  overflow-y: auto;
  padding-bottom: 20px;
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 20px;
    font-weight: 700;
    margin: 0;

    color: #d0011b;
    font-size: 30px;
    font-weight: 700;
  }
`;

const Close = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 50%;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);

  padding: 1px 10px;
  color: #ffffff;
  font-size: 30px;
  font-weight: 400;
  text-transform: uppercase;
`;

// basic card component:
const Card = styled.div`
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 20px;
  background-color: #ffffff;
  overflow: hidden;
`;

const LocationStyle = styled(Card)`
  margin-top: 35px;
  padding: 15px 21px;
`;

const LocationItem = styled.div`
  color: #4a494a;
  font-size: 13px;
  line-height: 30px;

  display: flex;
  justify-content: space-between;

  &:not(:last-child) {
    margin-bottom: 10px;
  }

  & span:last-child {
    color: #d0011b;
    font-size: 13px;
    font-weight: 700;
  }
`;

const TimeOfferPickerStyle = styled(Card)`
  margin: 14px 0;
  padding: 12px 33px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    color: #9f9c9c;
    font-size: 12px;
    font-weight: 700;
  }
`;

const FilterStyle = styled(Card)`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
  height: 40px;

  & div:first-child {
    border-radius: 100px;
    padding: 0 16px;
    background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
    font-size: 15px;
    font-weight: 700;
    line-height: 41px;
    color: #ffffff;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  }

  & div:last-child {
    padding: 10px;
    font-size: 15px;
    font-weight: 700;
    display: flex;
    align-items: center;

    i {
      font-size: 20px;
      opacity: 0.5;
      margin-left: 10px;
    }
  }
`;

const ListOfDealsNoMorning = () => {
  return (
    <Body>
      <Header>
        <Close>&times;</Close>
        <h1>pepper</h1>
        <span>&nbsp;</span>
      </Header>

      <LocationCard />

      <TimeOfferPicker active="Evening" />

      <Filter />

      <MainContent />
    </Body>
  );
};

export default ListOfDealsNoMorning;

function LocationCard() {
  return (
    <LocationStyle>
      <LocationItem>
        <span>
          <b>Showing:</b> Nearby
        </span>
        <span>Change Location</span>
      </LocationItem>

      <LocationItem>
        <span>
          <b>Date:</b> Tues, Aug 17th 2018
        </span>
        <span>Change Date</span>
      </LocationItem>

      <LocationItem>
        <span>
          <b>Pre-Payment:</b> $100
        </span>
        <span>Change Pre-Payment</span>
      </LocationItem>
    </LocationStyle>
  );
}

function TimeOfferPicker({ active }) {
  const activeStyle = {
    color: "#d0011b",
    borderBottom: "2px solid #d0011b",
    paddingBottom: 2
  };
  return (
    <TimeOfferPickerStyle>
      {["All Day", "All", "Morning", "Afternoon", "Evening"].map(time => (
        <span key={time} style={active === time ? activeStyle : {}}>
          {time}
        </span>
      ))}
    </TimeOfferPickerStyle>
  );
}

function Filter() {
  return (
    <FilterStyle>
      <div>Tues, Aug 17th, 2018 Offers</div>
      <div>
        <span>Filter</span>
        {/* you may wanna use a react native icon pack or svg icon */}
        <i className="fa fa-spotify" />
      </div>
    </FilterStyle>
  );
}

const MainContentStyle = styled.div`
  h3 {
    color: #4a494a;
    font-size: 12px;
    font-weight: 500;
    text-align: right;
    margin-bottom: 15px;
  }

  & > div:not(:last-child) {
    margin-bottom: 8px;
  }
`;

const NoOffers = styled(Card)`
  padding: 22px 13px;
  h2 {
    margin-bottom: 10px;
    font-size: 20px;
  }
  p {
    font-weight: 400;
    font-size: 14px;
  }
`;

function MainContent() {
  return (
    <MainContentStyle>
      <h3>Listing by Nearest Restaurant</h3>

      <NoOffers>
        <h2>No Morning Offers Available</h2>
        <p>
          Sorry, there are no evening offers available for the parameters you
          selected. Try widening your search filters or selecting a different
          date.
        </p>
      </NoOffers>
    </MainContentStyle>
  );
}
