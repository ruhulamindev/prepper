import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/free-solid-svg-icons";

// edit this to fit your needs
const Body = styled.div`
  width: 385px;
  height: 667px;
  padding: 1px 23px;
  background: #fff;

  font-family: "Avenir Next" ;

  /* just for the browser */
  box-shadow: 0 5px 22px 2px rgba(128, 128, 128, 0.17);
`;

const Header = styled.div`
  width: 100%;
  height: 27px;
  color: #d0011b;
  display: flex;
  margin-top: 17px;
  align-items: center;
  justify-content: space-between;

  svg {
    margin-left: 10px;
  }

  h1 {
    font-size: 20px;
    font-weight: 700;
    margin: 0;
  }
`;

const Main = styled.main`
  width: 100%;
  margin-top: 50px;
  text-align: center;

  h2 {
    color: #000000;
    font-size: 15px;
    font-weight: 700;
    margin-bottom: 40px;
  }
`;

const Button = styled.div`
  width: 311px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.04);
  border-radius: 11px;
  background-image: linear-gradient(180deg, #d0011b 0%, #fc445b 100%);
  display: inline-block;
  color: #ffffff;
  font-size: 22px;
  font-weight: 700;
  padding: 16px 0;
`;

const Input = styled.input`
  width: 311px;
  padding: 21px;
  border: none;
  border-radius: 11px;
  background-color: #efefef;
  color: #000000;
  font-size: 17px;
  font-weight: 700;

  &:focus {
    border: none;
    outline: none;
  }
`;

const ChangePassWord = () => {
  return (
    <Body>
      <Header>
        <FontAwesomeIcon icon={faChevronLeft} />
        <h1>Password</h1>
        <span>&nbsp;</span>
      </Header>

      <Main>
        <h2>Enter new password below.</h2>
        <Input placeholder="New Password" />
        <Input
          style={{ marginTop: 18 }}
          placeholder="Enter New Password Again"
        />
        <Button style={{ marginTop: 28 }}>Change Password</Button>
      </Main>
    </Body>
  );
};

export default ChangePassWord;
