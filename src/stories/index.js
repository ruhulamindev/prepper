import React from "react";
import { storiesOf } from "@storybook/react";

import "./index.css";
import "font-awesome/css/font-awesome.min.css";

import EnableNotification from "../pages/enableNotification";
import ChangePassWord from "../pages/ChangePassWord";

import DisableNotification from "../pages/disableNotification";
import SetPassWord from "../pages/setPassword";
import ListOfDealsEvening from "../pages/listOfDealsWithTimeOffersEvening";
import ListOfDealsMorning from "../pages/listOfDealsWithTimeOffersMorning";
import ListOfDealsAll from "../pages/ListOfDealsAll";
import ListOfDealsAllDay from "../pages/ListOfDealsAllDay";
import ListOfDealsNoEvening from "../pages/ListOfDealsNoEvening";
import ListOfDealsNoMorning from "../pages/ListOfDealsNoMorning";
import EditPaymentMethod from "../pages/editPaymentMethod";
import SelectPaymentMethod from "../pages/selectPaymentMethod";
import SelectPaymentMethodNoCards from "../pages/selectPaymentMethodNoCard";
import SummeryOfPayment from "../pages/summeryOfPayment";
import About from "../pages/about";
import Login from "../pages/login";
import SignUp from "../pages/signup";
import AddCard from "../pages/addcard";
import CvcInfo from "../pages/cvcInfo";
import Notification from "../pages/notification";
import EnableLocation from "../pages/enableLocation";
import FirstScreen from "../pages/firstScreen";
import SelectDate from "../pages/selectDate";
import ResetPassWord from "../pages/resetpassword";
import Homescreen from "../pages/homescreen";
import PrePayment from "../pages/prepayment";
import Profile from "../pages/profile";
import ViewPurchase from "../pages/viewpurchase";
import PurchaseDeal from "../pages/purchaseDeal";
import GetFreeMoney from "../pages/getFreeMoney";
import PurchaseCode from "../pages/purchaseCode";
import ViewMenu from "../pages/viewMenu";
import SignUpUserName from "../pages/ signUpUserName";

const center = storyfn => (
  <div
    style={{
      boxSizing: "border-box",
      maxWidth: "414px",
      minHeight: "650px",
      margin: "2rem auto",
      fontFamily:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
    }}
  >
    {storyfn()}
  </div>
);

storiesOf("notification", module)
  .addDecorator(center)
  .add("enable notification", () => <EnableNotification />)
  .add("disable notification", () => <DisableNotification />)
  .add("Notification", () => <Notification />)
  .add("enable location", () => <EnableLocation />);

storiesOf("log in", module)
  .addDecorator(center)
  .add("Change password", () => <ChangePassWord />)
  .add("Set password", () => <SetPassWord />);

storiesOf("List of deals", module)
  .addDecorator(center)
  .add("evening offers", () => <ListOfDealsEvening />)
  .add("morning offers", () => <ListOfDealsMorning />)
  .add("All offers", () => <ListOfDealsAll />)

  .add("All Day offers", () => <ListOfDealsAllDay />)
  .add("Evening No offers", () => <ListOfDealsNoEvening />)
  .add("Morning No offers", () => <ListOfDealsNoMorning />)
  .add("view purchase", () => <ViewPurchase />);

storiesOf("payment related", module)
  .addDecorator(center)
  .add("edit payment method", () => <EditPaymentMethod />)
  .add("select payment method", () => <SelectPaymentMethod />)
  .add("Summery of payment", () => <SummeryOfPayment />)
  .add("add a card", () => <AddCard />)
  .add("select payment method - no cards", () => <SelectPaymentMethodNoCards />)
  .add("Cvc info", () => <CvcInfo />)
  .add("enter prepayment", () => <PrePayment />)
  .add("purchase", () => <PurchaseDeal />)
  .add("purchase code", () => <PurchaseCode />);

storiesOf("main page", module)
  .addDecorator(center)
  .add("about", () => <About />)
  .add("home", () => <Homescreen />)
  .add("profile", () => <Profile />);

storiesOf("autentication", module)
  .addDecorator(center)
  .add("login", () => <Login />)
  .add("sign up", () => <SignUp />)
  .add("first screen", () => <FirstScreen />)
  .add("reset password", () => <ResetPassWord />)
  .add("sign up user name", () => <SignUpUserName />);

storiesOf("others", module)
  .addDecorator(center)
  .add("select date", () => <SelectDate />)
  .add("get free money", () => <GetFreeMoney />)
  .add("view menu", () => <ViewMenu />);
