import React from "react";
import ReactDOM from "react-dom";
import EnableNotification from "./pages/enableNotification";

const App = () => {
  return (
    <div>
      <EnableNotification />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
